'use strict';

var gulp = require('gulp'),
watch = require('gulp-watch'),
prefixer = require('gulp-autoprefixer'),
uglify = require('gulp-uglify'),
sass = require('gulp-sass'),
sourcemaps = require('gulp-sourcemaps'),
rigger = require('gulp-rigger'),
cssmin = require('gulp-minify-css'),
imagemin = require('gulp-imagemin'),
pngquant = require('imagemin-pngquant'),
rimraf = require('rimraf'),
browserSync = require("browser-sync"), 
notify = require("gulp-notify"),
ftp = require('gulp-ftp'),
gutil = require('gulp-util'),
bower = require('gulp-bower'),
changed = require('gulp-changed'),
wait = require('gulp-wait2'),

bower = require('gulp-bower'),
reload = browserSync.reload;
var path = {
    server: {
        theme: 'server/' 
    },
    build: {
        html: 'mozo2/',
        theme: 'build/' 
    },
    src: {
         html: 'mozo2/*.html',
        theme: 'mozo2/**/*' 
    },
    watch: {
        html: 'mozo2/*.html',
        theme: 'mozo2/**/*'  
    },
    clean: './build'
};

/*==============================================*/
var config = {
    server: {
        baseDir: "./mozo2"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend_Devil"
};

gulp.task('webserver', function () {
    browserSync(config);
});
gulp.task('html:build', function () {
    gulp.src(path.src.html) 
       // .pipe(rigger())
        //.pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});
/*==============================================*/



gulp.task('deploy', function () {
    gulp.src(path.src.theme).pipe(changed('server'))
    
    .pipe(ftp({
        host: 'shopon.ftp.tools',
        user: 'shopon_mozo2',
        pass: 'hvy1647r',
        remotePath: '/'
    }))
    .pipe(gutil.noop());
    gulp.src(path.src.theme)
    .pipe(gulp.dest('server/'));
});

gulp.task('stylebuild', function () {
    gulp.src(['mozo2/css/src/style.scss', 'mozo2/css/src/display.scss'])  
        //.pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(sass({
            sourceMap: true,
            errLogToConsole: false
        }))
        .on('error', function(err) {
            notify().write(err);
            this.emit('end');
        })
        .pipe(prefixer())
        //.pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('mozo2/css/'))
        .pipe(reload({stream: true}));
    });


gulp.task('donestyle', function() {
    gulp.src('src/style/style.scss')
    .pipe(notify("Стили обновлены"));
    
});

gulp.task('build', [ 
    'html:build',
    'deploy'
    ]);


gulp.task('watch', function(){
    watch([path.watch.theme], function(event, cb) {
       gulp.start('deploy');
   });
    watch(['mozo2/css/src/style.scss', 'mozo2/css/src/display.scss'], function(event, cb) {
       gulp.start('stylebuild');
       gulp.start('deploy');
   });
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
});


gulp.task('default', ['build', 'webserver', 'watch' ]);

